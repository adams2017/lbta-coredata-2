//
//  AppDelegate.swift
//  lbta-kindle-2
//
//  Created by Admin on 8/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit
//
//GLOBAL PUBLIC THEME
public var axTheme:AXThemeFactory.AXTheme!
//
//GLOBAL PUBLIC THEME
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    // 1) THEME VARIABLE  var axTheme:AXThemeFactory.AXTheme?
    //var axTheme:AXThemeFactory.AXTheme?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // CREATE THEME
        axTheme = AXThemeFactory.build(.core)
        
        if axTheme == nil {
            fatalError("Theme could not be built" )
        }
        
        window = window ?? UIWindow()
        window?.makeKeyAndVisible()
        
        let rootViewController = CompanyListViewController()
        let nav = NavigationControllerWithStatusStyleFromTheme(rootViewController: rootViewController)
        // let nav = UINavigationController(rootViewController: rootViewController)
        window?.rootViewController = nav
        // STYLE THE NAV BAR W/ STHEME
        setupNavigationBarStyle()
        return true
    }
    
    // SETS NAV BAR STYLE FROM THEM
    func setupNavigationBarStyle() {
        UINavigationBar.appearance().barTintColor = axTheme?.color(of: .navigationBarBarTint)
        UINavigationBar.appearance().tintColor = axTheme?.color(of: .navigationBarTint)
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor : axTheme?.color(of: .navigationBarLargeTitleTextAttributeForeground) as Any]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: axTheme?.color(of: .navigationBarTitleTextAttributeForeground) as Any]
    }
    
}


