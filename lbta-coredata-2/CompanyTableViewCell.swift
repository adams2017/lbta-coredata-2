
import UIKit
class CompanyTableViewCell: UITableViewCell {
    static var  reuseIdentifier : String  {
        return String(describing: self )
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // Enable it for editing when it gets an editing accessory
  //  override func didTransition(to state: UITableViewCell.StateMask) {
   //     self.textField.isEnabled = state.contains(.showingEditControl)
   //     super.didTransition(to:state)
  //  }
    
    public var aCompany:Company? {
        didSet {
            guard let aCompany = aCompany  else {
                fatalError("nill entity passed to cell ")
            }
            if aCompany.imageData != nil {
             companyImageView.image = UIImage(data: (aCompany.imageData!))
            }
            var date = ""
            if let fDate = aCompany.founded {
                let formatter = DateFormatter()
                formatter.dateFormat = "M/d/y"
                date  = formatter.string(from: fDate)
            } else {
                date = "Unknown"
            }
            nameAndDateLabel.text = aCompany.name! + " - Founded: " + date
        }
        
    }
        
    func setup() {
        self.backgroundColor = axTheme.color(of: .tableViewCellBackground)
        setupConstraints()
    }
    func setupConstraints() {
        [companyImageView, nameAndDateLabel].forEach {
            self.contentView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints=false
        }
        NSLayoutConstraint.activate([
            companyImageView.heightAnchor.constraint(equalToConstant: 40),
            companyImageView.widthAnchor.constraint(equalToConstant: 40),
            companyImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            companyImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 4),
            
            nameAndDateLabel.topAnchor.constraint(equalTo: self.topAnchor),
            nameAndDateLabel.leftAnchor.constraint(equalTo: companyImageView.rightAnchor, constant: 4),
            nameAndDateLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            nameAndDateLabel.rightAnchor.constraint(equalTo: self.rightAnchor)
            
            ])
    }

    let companyImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = #imageLiteral(resourceName: "icons8-question_mark")
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        ui.layer.cornerRadius = 20
        ui.layer.borderWidth = 1
        ui.layer.borderColor = axTheme.color(of: .imageViewLayerBorder).cgColor
        
        return ui
    }()
    
    let nameAndDateLabel:UILabel = {
        let ui = UILabel()
        ui.text = "name and date label"
        ui.font = axTheme.font(of: .labelText)
        ui.textColor = axTheme.color(of: .tableViewCellText)
       // ui.backgroundColor = UIColor.white
        ui.numberOfLines = 1
        return ui
    }()
}



