//
//  EmployeeRole.swift
//  lbta-coredata-2
//
//  Created by Admin on 9/4/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation

enum EmployeeRole:String, CaseIterable {
    case executive
    case developer
    case intern
    
}
