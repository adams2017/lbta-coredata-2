//
//  CreateCompanyViewController.swift
//  lbta-coredata-2
//
//  Created by Admin on 8/28/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit
import CoreData


protocol  MaintainCompanyAble {
    func addCompany(aCompany:Company)
    func changeCompany(aCompany:Company)
}


class CompanyDetailsViewController: UIViewController,
                UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    let nameLabel:UILabel = {
        let ui = UILabel()
        ui.text = "Name"
       // ui.font = UIFont.systemFont(ofSize: <#SIZE#>)
        ui.textColor = UIColor.black
        ui.backgroundColor = axTheme.color(of: .viewBackground)
        ui.numberOfLines = 1
        return ui
    }()
    let nameTextField:UITextField  = {
        let ui = UITextField()
        ui.placeholder = "Enter name"
        ui.backgroundColor = axTheme.color(of: .viewBackground)
       // ui.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
       // ui.leftViewMode = UITextField.ViewMode.always
       // ui.layer.borderColor = UIColor.lightGray.cgColor
        //ui.layer.borderWidth = 1
        //ui.layer.cornerRadius = 16
        return ui
    }()
    
    let foundedDatePicker:UIDatePicker = {
        let ui = UIDatePicker()
        ui.datePickerMode = .date
        return ui
    }()
    let backgroundView:UIView = {
       let ui = UIView()
        ui.backgroundColor = axTheme.color(of: .viewBackground)
        return ui
    }()
    
    lazy var companyImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = #imageLiteral(resourceName: "icons8-question_mark")
        ui.clipsToBounds = true
       ui.contentMode = UIView.ContentMode.scaleAspectFill
      //  ui.layer.cornerRadius = ui.frame.width / 2
        ui.isUserInteractionEnabled = true
        ui.addGestureRecognizer(
            UITapGestureRecognizer(target: self,
                                   action: #selector(companyImageTapGestureSelector)))
        return ui
    }()
    
    lazy var imagePickerController:ImagePickerControllerWithStatusStyleFromTheme = {
        let ui = ImagePickerControllerWithStatusStyleFromTheme()
        ui.allowsEditing = true
       return ui
    }()
    
    @objc func companyImageTapGestureSelector() {
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil) // we are the presenting controler, so we dismiss the pressented image picker
    }
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
  
        if let editedImage = info[.editedImage]  as? UIImage{
            companyImageView.image = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            companyImageView.image = originalImage
        }
        companyImageView.layer.cornerRadius = companyImageView.frame.width / 2
        companyImageView.clipsToBounds = true
        
        dismiss(animated: true, completion: nil)
    }
    var company:Company? {
        didSet {
            nameTextField.text = company?.name
            guard let date = company?.founded else { return }
            foundedDatePicker.date = date
            if let aJPEG = company?.imageData {
                companyImageView.image = UIImage(data: aJPEG)
                companyImageView.clipsToBounds = true
                companyImageView.layer.cornerRadius = 50
                print(companyImageView.frame)
                print(companyImageView.frame.width/2)
                companyImageView.layer.borderWidth = 2
                companyImageView.layer.borderColor = axTheme.color(of: .viewControllerBackground).cgColor
            } else {
                companyImageView.image = #imageLiteral(resourceName: "icons8-question_mark")
                companyImageView.layer.cornerRadius =  50
                companyImageView.clipsToBounds = true
            }
        }
    }
    
    var delegate:MaintainCompanyAble?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
        setup()
        setupConstraints()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = company == nil ? "New Company" : "Change Company"
    }
    
    func setup() {
        
        self.view.backgroundColor = axTheme.color(of: .viewControllerBackground)
       // navigationItem.title = "Company Maintenance"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneBarButtonTouchSelector))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonTouchSelector))
        //selector for touch

    }
    @objc func saveButtonTouchSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
   
        if company == nil {
            saveNew()
        } else {
            saveExisting()
        }
   
    }
    
    func saveNew() {
        guard let name = self.nameTextField.text else { return }
        let entity = NSEntityDescription.insertNewObject(
            forEntityName: "Company",
            into: CoreDataMgr.shared.pc.viewContext )
        entity.setValue(name, forKey: "name")
        entity.setValue(foundedDatePicker.date, forKey: "founded")
        
        if let image = companyImageView.image {
            let aJPEG = image.jpegData(compressionQuality: 0.8)
            entity.setValue(aJPEG, forKey: "imageData")
        }
        
        do {
            try CoreDataMgr.shared.pc.viewContext.save()
        } catch let err {
            let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
            print("Error: \(err)")
        }
        dismiss(animated: true) {
            self.delegate?.addCompany(aCompany: entity as! Company)
        }
        
    }
    func saveExisting() {
        // THE COMPANY ALREADY EXISTS IN THE ARRAY
        company?.name = nameTextField.text
        company?.founded = foundedDatePicker.date
        if let image = companyImageView.image {
            let aJPEG = image.jpegData(compressionQuality: 0.8)
            company?.imageData = aJPEG
        }
        
        do {
            try CoreDataMgr.shared.pc.viewContext.save()
        } catch let err {
            let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
            print("Error: \(err)")
        }
        
        dismiss(animated: true) {
            self.delegate?.changeCompany(aCompany: self.company!)
        }
    }
    //selector for touch
    @objc func doneBarButtonTouchSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
    }
    func setupConstraints() {
        
        [backgroundView,companyImageView,nameLabel, nameTextField, foundedDatePicker].forEach {
            self.view.addSubview($0); $0.translatesAutoresizingMaskIntoConstraints=false}
        
        NSLayoutConstraint.activate([
            
            backgroundView.topAnchor.constraint(equalTo: self.view.topAnchor),
            backgroundView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            backgroundView.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            backgroundView.heightAnchor.constraint(equalToConstant: 350),
            
            companyImageView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 16),
            companyImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            companyImageView.heightAnchor.constraint(equalToConstant: 100),
            companyImageView.widthAnchor.constraint(equalToConstant: 100),
            
            
            nameLabel.topAnchor.constraint(equalTo: companyImageView.bottomAnchor, constant: 8),
            nameLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 8),
            nameLabel.widthAnchor.constraint(equalToConstant: 100),
            nameLabel.heightAnchor.constraint(equalToConstant: 50),
            
            nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor),
            nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor),
            nameTextField.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            nameTextField.heightAnchor.constraint(equalTo: nameLabel.heightAnchor),
            
            foundedDatePicker.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: 8),
            foundedDatePicker.leftAnchor.constraint(equalTo: view.leftAnchor),
            foundedDatePicker.rightAnchor.constraint(equalTo: view.rightAnchor),
            foundedDatePicker.heightAnchor.constraint(equalToConstant: 150)
            
            ])
    }
 
    
}

