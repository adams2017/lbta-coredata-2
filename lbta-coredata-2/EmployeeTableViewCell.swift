import UIKit
class EmployeeTableViewCell: UITableViewCell {
    static var  reuseIdentifier : String  {
        return String(describing: self )
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  
    
    var aEmployee:Employee? {
        didSet {
            guard let aEmployee = aEmployee else {
                fatalError("nill Entity")
            }
            let name = aEmployee.name ?? "Unknown"
            let company = aEmployee.company?.name ?? "Unknown"
            aLabel.text =  name + " " + company
        }
        
    }
    func setup() {
        self.backgroundColor = axTheme.color(of: .tableViewCellBackground)
        setupConstraints()
    }
    func setupConstraints() {
        [aLabel].forEach {
            self.contentView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints=false
        }
        NSLayoutConstraint.activate([
            aLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 4),
            aLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 4),
            aLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 4),
            aLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 4)
            ])
    }

    let aLabel:UILabel = {
        let ui = UILabel()
        ui.text = ""
        ui.font = axTheme.font(of: .labelText)
        ui.textColor = axTheme.color(of: .tableViewCellText)
      //  ui.backgroundColor = axTheme.color(of: .tableViewCellBackground)
        ui.numberOfLines = 1
        return ui
    }()

}

