//
//  EmployeeTableViewController.swift
//  lbta-coredata-2
//
//  Created by Admin on 9/3/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class EmployeeListViewController: UITableViewController, EmployeeMaintainable {

    var aCompany:Company?
    
    var arrEmployees = [Employee]()
    
    struct Section  {
        var header:String
        var arrEmployee = [Employee]()
    }
    var  arrSection = [Section]()  // [0] IS FOR EMUM IX 0, [1] IS FOR ENUM IX 1
    
    func changeEmployee(changedEmployee: Employee) {
        arrSection.removeAll()
        fetchAllEmployees()
        tableView.reloadData()
        dismiss(animated: true) {
            
        }
        return
        
        
      // dismiss(animated: true) {
       //     self.tableView.reloadRows(at: [IndexPath(row: rowChanged, section: ix)], with: .automatic)
       // }
    
        
        // determine section and row
        for (ix, s) in arrSection.enumerated() {
            // HOW DO WE MOVE THE EMPLOYEE TO ANOTHER ROLE/SECTION ? OTHERWISE IT STAYS IN SAME
            // WE MAY HAVE TO REFETCH ???
            // IF WE FIND IT IN ARRSECTION, THAT IS THE OLD POSITION
            // THE changedEmployee role is always the current role.
            // so, find it in arrSection, if found section NE current section,
            // remove from old, add to new. and remove/add  table row/sect
            // OR do a NEW FETCH ???
            if let rowChanged = s.arrEmployee.firstIndex(of: changedEmployee) {
                // WE FOUND THE ORIGINAL SECTION. IS IT SAME AS CURRENT SECTION?
                // IF IT IS, RELAD IT
                // IF NOT, UPDATE THE MODEL AND THE VIEW . REMOVE IT FROM OLD, APPEND IT TO NEW
                let originalRole = s.header
            
                print("Original role ", s.header)
                print("Current role ", changedEmployee.role)
                
                if originalRole ==  changedEmployee.role {
                    dismiss(animated: true) {
                        self.tableView.reloadRows(at: [IndexPath(row: rowChanged, section: ix)], with: .automatic)
                     }
                } else {
                    //  remove old from model in old section, because row is still in old section
                    arrSection[ix].arrEmployee.remove(at: rowChanged)
                    // add to model
                    
                    // remove from tv
                    let ip = IndexPath(row: rowChanged, section: ix)
                    dismiss(animated: true) {
                        self.tableView.deleteRows(at: [ip], with: .middle)
                    }
                    
                }
                
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
        
        let vc = EmployeeDetailsViewController()
        vc.aEmployee = arrSection[indexPath.section].arrEmployee[indexPath.row]
        vc.delegate = self
        
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
        
    }
    
    
    func addEmployee(newEmployee: Employee) {
        #warning("save the employee ")
       // arrEmployees.append(newEmployee)
        // add to model then refresh
        for (ix, s) in arrSection.enumerated() {
            if  s.header == newEmployee.role {
                arrSection[ix].arrEmployee.append(newEmployee)
                break
            }
        }
        
        tableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup() {
        
        navigationItem.title = (aCompany?.name ?? "Unknown") +  " Employees"
        tableView.backgroundColor = axTheme.color(of: .viewControllerBackground)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus"), style: .plain, target: self, action: #selector(addEmployeeBarButtonTouchSelector))
        tableView.register(EmployeeTableViewCell.self, forCellReuseIdentifier: EmployeeTableViewCell.reuseIdentifier)
      
        fetchAllEmployees()

    }
    
    func fetchAllEmployees() {
        
        guard let companyEmployees = aCompany?.employees?.allObjects as? [Employee] else { return }
         // WE HAVE ALL APPLE EMPLOYEES, NOW SEGREGATE BY SECTION
        
        self.arrEmployees = companyEmployees // all in 1 pile
        // group by section
        let sortedArrEmployess = arrEmployees.sorted { $0.name! < $1.name! }
        // make a dictionary grouped
        let groupedBySection = Dictionary(grouping: sortedArrEmployess) {$0.role }
        
        for (_,val) in EmployeeRole.allCases.enumerated() {
            var  emps = groupedBySection[val.rawValue]
            if emps == nil {
                emps = [Employee]()
            }
            let s = Section(header: val.rawValue, arrEmployee: emps!)
            arrSection.append(s)
        }
        
    }
    
    @objc func addEmployeeBarButtonTouchSelector() {
        let vc = EmployeeDetailsViewController()
        vc.delegate = self
        vc.aCompany = self.aCompany
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return arrSection.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrSection[section].arrEmployee.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeTableViewCell.reuseIdentifier, for: indexPath) as! EmployeeTableViewCell
        // Configure the cell...
        cell.aEmployee = arrSection[indexPath.section].arrEmployee[indexPath.row]
        return cell
    }
    
  //  override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
  //      return UIView()
  //  }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return arrSection[section].arrEmployee.count == 0 ?  0 : 30
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrSection[section].header
    }

    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
