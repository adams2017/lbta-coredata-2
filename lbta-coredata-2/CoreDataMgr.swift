//
//  CoreDataManager.swift
//  lbta-coredata-2
//
//  Created by Admin on 8/29/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation
import CoreData

struct CoreDataMgr {
    static let shared = CoreDataMgr()
    // A container pre-loaded with the model
    
    let pc:NSPersistentContainer = {
       let container = NSPersistentContainer(name: "MyModel")
       container.loadPersistentStores(completionHandler: { (desc, err) in
            if let err = err {
                fatalError("Loading of store faile \(err)")
            }
        })
        return container
    }()
    
    func fetchAllCompanyEntities() -> [Company] {
        let request = NSFetchRequest<Company>(entityName: "Company")
        do {
            let arrEntities =  try CoreDataMgr.shared.pc.viewContext.fetch(request)
            return arrEntities
        } catch {
            return []
        }
    }
    
    func fetchAllEmployeeEntities() -> [Employee] {
        let request = NSFetchRequest<Employee>(entityName: "Employee")
        do {
            let arrEntities =  try CoreDataMgr.shared.pc.viewContext.fetch(request)
            return arrEntities
        } catch {
            return []
        }
    }
    
    func saveNewEmployeeEntity(name:String, dateOfBirth:Date , company:Company , role:String ) throws -> Employee  {
        let entity = NSEntityDescription.insertNewObject(
                        forEntityName: "Employee",
                        into: CoreDataMgr.shared.pc.viewContext ) as! Employee
        
        entity.name = name
        entity.dateOfBirth = dateOfBirth
        entity.company = company
        entity.role = role 
        do {
            try CoreDataMgr.shared.pc.viewContext.save()
            return entity
        } catch  {
            throw error
        }
    }
    
    func saveChangedEmployeeEntity() throws {
        
        do {
            try CoreDataMgr.shared.pc.viewContext.save()
        } catch  {
            throw error
        }
        
    }
    
    func resetDatabase() {
        
        var  deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Company")
        var deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try CoreDataMgr.shared.pc.viewContext.execute(deleteRequest)
            try CoreDataMgr.shared.pc.viewContext.save()
        } catch {
            print ("There was an error")
        }
        
         deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Employee")
         deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try CoreDataMgr.shared.pc.viewContext.execute(deleteRequest)
            try CoreDataMgr.shared.pc.viewContext.save()
        } catch {
            print ("There was an error")
        }
        
    }
}
