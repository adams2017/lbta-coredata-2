//
//  EmployeeMaintenanceViewController.swift
//  lbta-coredata-2
//
//  Created by Admin on 9/3/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

protocol EmployeeMaintainable {
    func addEmployee(newEmployee:Employee)
    func changeEmployee(changedEmployee: Employee)
}

class EmployeeDetailsViewController: UIViewController {

    var delegate:EmployeeMaintainable?
    var aCompany:Company?
    var aEmployee:Employee? {
        
        didSet {
            nameTextField.text = aEmployee?.name
            //dateOfBirthDatePicker.date = aEmployee?.dateOfBirth ?? Date()
            let aDateFormatter = DateFormatter()
            aDateFormatter.dateFormat = "MM/dd/yyyy"
            dateOfBirthTextField.text = aDateFormatter.string(from: aEmployee?.dateOfBirth ?? Date() )
            
            for (ix, role) in EmployeeRole.allCases.enumerated() {
                if role.rawValue == aEmployee?.role {
                    roleSegmentedControl.selectedSegmentIndex = ix
                    break
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupConstraints()
        
    }
    
    func setup() {
        navigationItem.title = "Employee Details"
        self.view.backgroundColor = axTheme.color(of: .viewControllerBackground)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTouchSelector))
 
    }
    
    func setupConstraints() {
        
        [panel,nameLabel, nameTextField, dateOfBirthLabel, dateOfBirthTextField, roleSegmentedControl ].forEach {
            self.view.addSubview($0); $0.translatesAutoresizingMaskIntoConstraints=false}
        
        NSLayoutConstraint.activate([
            panel.topAnchor.constraint(equalTo: self.view.topAnchor),
            panel.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            panel.heightAnchor.constraint(equalToConstant: 250),
            panel.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            
            nameLabel.topAnchor.constraint(equalTo: self.view.topAnchor),
            nameLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            nameLabel.heightAnchor.constraint(equalToConstant: 30),
            nameLabel.widthAnchor.constraint(equalToConstant: 50),
            
            nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor),
            nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor, constant: 0),
            nameTextField.heightAnchor.constraint(equalToConstant: 30),
            nameTextField.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            
            dateOfBirthLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 0),
            dateOfBirthLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor, constant: 0),
            dateOfBirthLabel.heightAnchor.constraint(equalToConstant: 30),
            dateOfBirthLabel.widthAnchor.constraint(equalToConstant: 50),
            
            dateOfBirthTextField.topAnchor.constraint(equalTo: dateOfBirthLabel.topAnchor, constant: 0),
            dateOfBirthTextField.leftAnchor.constraint(equalTo: nameTextField.leftAnchor, constant: 0),
            dateOfBirthTextField.heightAnchor.constraint(equalToConstant: 30),
            dateOfBirthTextField.rightAnchor.constraint(equalTo: nameTextField.rightAnchor, constant: 0),
            
            roleSegmentedControl.topAnchor.constraint(equalTo: dateOfBirthLabel.bottomAnchor, constant: 8),
           // roleSegmentedControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0),
            roleSegmentedControl.heightAnchor.constraint(equalToConstant: 30),
            roleSegmentedControl.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 8),
            roleSegmentedControl.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -8)
            
    
            ])
    }
  
    //selector for touch
    @objc func saveTouchSelector() {
        
        guard let name = nameTextField.text,
                  !name.isEmpty else { return }
        
        guard let dateOfBirth  = dateOfBirthTextField.text else {return}
  
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date  = dateFormatter.date(from: dateOfBirth)
        
        guard let dob = date else {
            let alert = UIAlertController(
                title: "Error!",
                message: "Invalid Date",
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true )
            return
        }
        
        if aEmployee == nil {
            let roleString = EmployeeRole.allCases[roleSegmentedControl.selectedSegmentIndex].rawValue
            
            do {
                let newEmployee = try  CoreDataMgr.shared.saveNewEmployeeEntity(name: nameTextField.text!,
                                                                                dateOfBirth: dob,
                                                                                company: aCompany!, role: roleString)
                delegate?.addEmployee(newEmployee: newEmployee)
            } catch let err {
                print("Error: \(err)")
            }
           
        } else {
            aEmployee?.name = nameTextField.text!
            aEmployee?.dateOfBirth = dob
            aEmployee?.role = EmployeeRole.allCases[roleSegmentedControl.selectedSegmentIndex].rawValue
            do {
                try   CoreDataMgr.shared.saveChangedEmployeeEntity()
            } catch let err {
                #warning("put an alert here ")
                print("Error: \(err)")
            }
            delegate?.changeEmployee(changedEmployee: aEmployee!)
            
        }
    }
    let panel:UIView = {
        let ui = UIView()
        ui.backgroundColor = axTheme.color(of: .viewBackground)
        return ui
    }()
    
    let nameLabel:UILabel = {
        let ui = UILabel()
        ui.text = "Name"
       // ui.font = UIFont.systemFont(ofSize: 16)
        //ui.textColor = axTheme.color(of: .labelText)
        ui.textColor = UIColor.black   // how to theme ?
        ui.font = axTheme.font(of: .labelText)
       // ui.backgroundColor = UIColor.white
        ui.numberOfLines = 1
        return ui
    }()
    
    let dateOfBirthLabel:UILabel = {
        let ui = UILabel()
        ui.text = "DOB"
        ui.font = axTheme.font(of: .labelText)
        // ui.font = UIFont.systemFont(ofSize: 16)
        ui.textColor = UIColor.black // how ot theme ?
        // ui.backgroundColor = UIColor.white
        ui.numberOfLines = 1
        return ui
    }()
    
    let nameTextField:UITextField  = {
        let ui = UITextField()
        ui.placeholder = "name"
       // ui.backgroundColor = UIColor.white
        ui.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        ui.leftViewMode = UITextField.ViewMode.always
      //  ui.layer.borderColor = UIColor.lightGray.cgColor
      //  ui.layer.borderWidth = 1
       // ui.layer.cornerRadius = 16
        return ui
    }()
    
    let dateOfBirthTextField:UITextField  = {
        let ui = UITextField()
        ui.placeholder = "mm/dd/yyyy"
        // ui.backgroundColor = UIColor.white
        ui.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        ui.leftViewMode = UITextField.ViewMode.always
        //  ui.layer.borderColor = UIColor.lightGray.cgColor
        //  ui.layer.borderWidth = 1
        // ui.layer.cornerRadius = 16
        return ui
    }()

    let roleSegmentedControl:UISegmentedControl = {
       // var roles = EmployeeRole.allCases.map {$0.rawValue }
        let ui = UISegmentedControl(items: EmployeeRole.allCases.map {$0.rawValue } )
        ui.tintColor = UIColor.black
        ui.selectedSegmentIndex = 0
        ui.sizeToFit()
        return ui
    }()
 
}

