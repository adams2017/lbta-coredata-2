//
//  ViewController.swift
//  lbta-coredata-2
//
//  Created by Admin on 8/28/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit
import CoreData

class CompanyListViewController: UITableViewController, MaintainCompanyAble {

    var arrCompany = [Company]()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = axTheme.color(of: .viewControllerBackground)
        navigationItem.title = "Companies"
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus"), style: .plain, target: self, action: #selector(addCompanyBarButtonTouchSelector))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(resetTouchSelector))
  
        tableView.register(CompanyTableViewCell.self, forCellReuseIdentifier: CompanyTableViewCell.reuseIdentifier)
        let arrEntities =  CoreDataMgr.shared.fetchAllCompanyEntities()
        self.arrCompany = arrEntities
        
        //self.tableView.reloadData()
        
    }
    //selector for touch
    @objc func resetTouchSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
   
        /*
        arrCompany.forEach { (aCompany) in
            CoreDataMgr.shared.pc.viewContext.delete(aCompany)
        }
        
        do {
            try CoreDataMgr.shared.pc.viewContext.save()
        } catch let err {
            print("Error: \(err)")
        }
 */
        CoreDataMgr.shared.resetDatabase()
        
        // IF YOU WANT ANIMATION, USE THE TV METHOD PASSING ARRAY OF INDICES
        arrCompany.removeAll()
        tableView.reloadData()
    }
    
    @objc func addCompanyBarButtonTouchSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
        
        let createCompanyViewController = CompanyDetailsViewController()
        createCompanyViewController.delegate = self
        let nav = NavigationControllerWithStatusStyleFromTheme(rootViewController: createCompanyViewController)
        present(nav, animated: true, completion: nil)

    }


    let footerLabel:UILabel = {
        let ui = UILabel()
        ui.text = "No Company Items Exist"
        ui.font = UIFont.systemFont(ofSize: 30)
        ui.textColor = UIColor.black
        ui.backgroundColor = axTheme.color(of: .viewBackground)
        ui.textAlignment =  .center
        ui.numberOfLines = 1
        return ui
    }()

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if arrCompany.count == 0{
            return footerLabel
}       else {
            return UIView()
        }
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
   
        return  arrCompany.count ==  0 ? 150 : 50
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCompany.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: CompanyTableViewCell.reuseIdentifier,
                                                  for: indexPath) as! CompanyTableViewCell
       // cell.backgroundColor =  axTheme.color(of: .tableViewCellBackground)
       // cell.textLabel?.textColor = axTheme.color(of: .labelText)
       // cell.textLabel?.font = axTheme.font(of: .labelText)
        let company = arrCompany[indexPath.row]
        cell.aCompany = company
        
        return cell
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let h = UIView()
        h.backgroundColor = axTheme.color(of: .tableViewHeaderBackground)
        return h
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50 
    }
    func addCompany(aCompany: Company) {
        // ADD IT TO MODEL
        arrCompany.append(aCompany)
        // CALCULATE PATH AT BOTTOM
        let ip = IndexPath(row: arrCompany.count-1, section:   0)
        // Add it to bottom of TV
        tableView.insertRows(at: [ip], with: .none) // inserts row from data source at ip into TV
        tableView.reloadData()
    }
    func changeCompany(aCompany: Company) {
        
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
    
        let rowOfChangedCompany = arrCompany.firstIndex(of: aCompany)!
        tableView.reloadRows(at: [IndexPath(row: rowOfChangedCompany, section: 0)],
                             with: .automatic)
        
    }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
 
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
            let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
            let company = self.arrCompany[indexPath.row]
            
            CoreDataMgr.shared.pc.viewContext.delete(company)
            do {
                try CoreDataMgr.shared.pc.viewContext.save()
            } catch let err {
                fatalError("\(err)")
            }
            
            self.arrCompany.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
    
            
            completion(true)
        }
        deleteAction.backgroundColor = axTheme.color(of: .tableViewDeleteActionBackground)
        // d.image = UIGraphicsImageRenderer(size:CGSize(30,30)).image { _ in
        //    UIImage(named:"trash")?.draw(in: CGRect(0,0,30,30))
        //}
        let editAction = UIContextualAction(style:  .normal, title: "Edit") {
            
            (action, view, completion) in
            
            let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
            
            let vc = CompanyDetailsViewController()
            vc.company = self.arrCompany[indexPath.row]
            vc.delegate = self
            let nav = NavigationControllerWithStatusStyleFromTheme(rootViewController: vc)
            self.present(nav , animated: true, completion: nil)
            completion(true)
        }
        
        
        editAction.backgroundColor = axTheme.color(of: .tableViewEditActionBackground)
        return UISwipeActionsConfiguration(actions: [deleteAction, editAction])
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
   
  
        let vc = EmployeeListViewController()
        vc.aCompany = arrCompany[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)

    }
    
}


