//
//  AXThemeFactory.swift
//  lbta-kindle-2
//
//  Created by Admin on 8/27/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

//
//  axthemekit.swift
//  test-axThemeKit
//
//  Created by Admin on 8/5/19.
//  Copyright ©
//
//  ThemeFactory.swift
//  theme2
//
//  Created by Admin on 8/3/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

// THIS EXTENSION TO USVIEWCONTROLLER ALLOWS EASIER ACCESS TO axAppTheme
//**********************************************************************
// REQUIRED EXTENSION METHODS                                               *
//**********************************************************************

/* add this extension to your project
 recommended file name:  UIViewController+AXThemeFactory
 import UIKit
 import AXThemeKit
 
 extension UIViewController {
 var axTheme: AXThemeFactory.AXTheme {
 let delegate = (UIApplication.shared.delegate as! AppDelegate)
 guard let theme = delegate.axTheme else {
 fatalError("Theme is nil !" )
 }
 return theme
 }
 }
 */
//  https://quicktype.io
// https://brandpalettes.com/all-brands/
// https://learnui.design/blog/ios-font-size-guidelines.html
// http://www.convertcsv.com/html-table-to-csv.htm

/* Usage instructions:
 In App Delegate:
 1) import UIKit
 2) var axTheme:AXThemeFactory.AXTheme
 3) axTheme = AXThemeFactory.build(. YOUR CHOICE )
 In Project folder
 4) Add the extension to UIViewController
 5) copy the framework, add the binary in the project properties
 
 */

import UIKit

// they would be called  axThemeCustom01...
// https://matteomanferdini.com/codable/
// coolers.co   for color samples
// https://quicktype.io

//Called in  AppDelegate


public class AXThemeFactory {
    
    private init() {}
    
    //stock style and cstom style
    public enum ThemeSelector :String {
        case basic
        case core
        case kindle
        case custom1 = "1"
        case custom2 = "2"
        case custom3 = "3"
        case custom4 = "4"
        case custom5 = "5"
        
        var fileName:String {
            get {
                return "ax_theme_" + self.rawValue
            }
        }
    }
    
    enum Category:String {
        case color = "Color"
        case gradient = "Gradient"
        case font = "Font"
    }
    
    public enum AXTheme {
        case visuals([String:Any])  //associated value is a dictionary
        
        public func color(of uielement:ColorElement) -> UIColor {
            let key = uielement.rawValue + Category.color.rawValue
            //check for null
            
            if case let .visuals(d) = self {
                if let value = d[key] {
                    return value as! UIColor
                } else {
                    return UIColor.gray
                }
            }
            return UIColor.red  // SHOULD NOT OCCURE
        }
        public func gradient(of uielement:GradientElement, bounds:CGRect) -> CAGradientLayer {
            let key = uielement.rawValue + Category.gradient.rawValue
            if case let .visuals(d) = self {
                if let value = d[key] {
                    let g = value as! CAGradientLayer
                    g.frame = bounds
                    return g
                } else {
                    //create a default grey gradient for non existent key
                    let g = CAGradientLayer()
                    g.locations = [0,1]
                    g.startPoint = CGPoint(x: 0, y: 0)
                    g.endPoint = CGPoint(x: 1, y: 1)
                    g.colors = [UIColor.lightGray.cgColor, UIColor.white.cgColor]
                    g.frame = bounds
                    return g
                }
                
            }
            return CAGradientLayer()  //should not occur
        }
        public func font(of uielement:FontElement) -> UIFont {
            if case let .visuals(d) = self {
                let key = uielement.rawValue + Category.font.rawValue
                if let value = d[key] {
                    return value as! UIFont
                } else {
                    return UIFont.systemFont(ofSize: 17) // a resonable default
                }
            }
            return UIFont.systemFont(ofSize: 50) // should not occur
        }
    }
    
    // THESE ARE THE COLOR ELEMENTS THAT CORRESPOND TO A VIEW PROPERTY
    public enum ColorElement:String, CaseIterable {
        case viewControllerBackground
        case viewBackground
        case buttonBackground
        case buttonTitle
        case buttonTint
        case navigationBarTint
        case navigationBarBarTint
        case navigationBarLargeTitleTextAttributeForeground
        case navigationBarTitleTextAttributeForeground
        case navigationBarStatus
        case navigationBarButtonItemTint
        case tableViewBackground
        case tableViewCellBackground
        case tableViewSeparator
        case tableViewFooterView
        case tableViewHeaderBackground
        case tableViewDeleteActionBackground
        case tableViewEditActionBackground
        case tableViewOtherActionBackground
        case tableViewCellText
        case labelBackground
        case labelText
        case segmentedControlTint
        case imageViewLayerBorder
    }
    
    // THESE ARE THE FONT ELEMENTS FOR A VIEW FONT PROPERTIES
    public enum FontElement:String, CaseIterable {
        case buttonTitle
        case navigationBarLargeTitleTextAttribute
        case navigationBarTitleTextAttribute
        case labelText
    }
    
    // THE GRADIENT ONLY APPLIES TO A VIEW
    public enum GradientElement:String, CaseIterable {
        case view
    }
    
    //MARK: - Theme for basic
    fileprivate static func buildThemeForBasic() -> AXTheme {
        var d = [String:Any]()
        for el in ColorElement.allCases {
            // key is <element>"Color"
            // ExampleL  "buttonTitleColor"
            // Value is the UIColor object
            let key = el.rawValue + Category.color.rawValue
            switch el  {
            case .viewControllerBackground: d[key] = UIColor.axCyan
            case .buttonBackground: d[key] = UIColor.blue
            case .buttonTitle: d[key] = UIColor.white
            case .navigationBarTint: d[key] = UIColor.red
            case .navigationBarBarTint: d[key] = UIColor.red
            case .navigationBarLargeTitleTextAttributeForeground: d[key] = UIColor.gray
            case .navigationBarTitleTextAttributeForeground: d[key] = UIColor.white
            case .tableViewBackground: d[key] = UIColor.orange
            case .tableViewCellBackground: d[key] = UIColor.gray
            case .tableViewSeparator: d[key] = UIColor.lightGray
            case .tableViewHeaderBackground: d[key] = UIColor.darkGray
            case .tableViewFooterView: d[key] = UIColor.blue
            case .labelBackground: d[key] = UIColor.darkGray
            case .labelText: d[key] = UIColor.yellow
            default: break
            }
        }
        
        for el in GradientElement.allCases {
            // key is <element>"Gradient"
            // Example:  "viewGradient"
            // value is a CAGradientLayer
            let key = el.rawValue + Category.gradient.rawValue
            let g = CAGradientLayer()
            g.colors = [UIColor.white.cgColor, UIColor.darkGray.cgColor ]
            g.locations = [0.0, 1.0]
            g.startPoint = CGPoint(x: 1, y: 1.0)
            g.endPoint = CGPoint(x: 0, y: 0)
            
            switch el  {
            case .view:
                d[key] = g
            }
        }
        
        for el in FontElement.allCases {
            // key is <element>"Font"
            // Example:  "ButtonTitleFont"
            // Value is the UIFont object
            let key = el.rawValue + Category.font.rawValue
            let f = UIFont.systemFont(ofSize: 18)
            switch el  {
            case .buttonTitle: d[key] = f
            case .labelText: d[key] = f
            case .navigationBarLargeTitleTextAttribute:  d[key] = f
            case .navigationBarTitleTextAttribute:  d[key] = f
            }
        }
        return AXTheme.visuals(d)
    }
    
    //MARK: - Theme for LBTA "CORE DATA" project
    fileprivate static func buildThemeForCore() -> AXTheme {
        var d = [String:Any]()
        //COLORS
        for el in ColorElement.allCases {
            let key = el.rawValue + Category.color.rawValue
            switch el  {
            case .viewControllerBackground: d[key] = UIColor.lbtaCoreBlue
            case .viewBackground: d[key] = UIColor.lbtaCoreLigtBlue
                
            case .buttonBackground: d[key] = UIColor.magenta
            case .buttonTitle: d[key] = UIColor.white
                
            case .navigationBarTint: d[key] = UIColor.white  // button image color, and set image to Transparent
            case .navigationBarBarTint: d[key] = UIColor.lbtaCoreRed
            case .navigationBarStatus: d[key] = UIColor.white
            case .navigationBarLargeTitleTextAttributeForeground: d[key] = UIColor.white
            case .navigationBarTitleTextAttributeForeground: d[key] = UIColor.white
                
            case .tableViewBackground: d[key] = UIColor.axSalmon1
            case .tableViewCellBackground: d[key] = UIColor.lbtaCoreTeal
            case .tableViewSeparator: d[key] = UIColor.black
            case .tableViewHeaderBackground: d[key] = UIColor.lbtaCoreLigtBlue
            case .tableViewDeleteActionBackground: d[key] = UIColor.lbtaCoreRed
            case .tableViewEditActionBackground: d[key] = UIColor.lbtaCoreBlue
            case .tableViewCellText: d[key] = UIColor.white
                #warning("is there a action text color too ? ")
            case .labelBackground: d[key] = UIColor.black
            case .labelText: d[key] = UIColor.white
            case .imageViewLayerBorder: d[key] = UIColor.lbtaCoreBlue
            default: break
            }
        }
        
        // GRADIENTS
        for el in GradientElement.allCases {
            let key = el.rawValue + Category.gradient.rawValue
            let g = CAGradientLayer()
            g.colors = [UIColor.white.cgColor, UIColor.orange.cgColor ]
            g.locations = [0.0, 1.0]
            g.startPoint = CGPoint(x: 1, y: 1.0)
            g.endPoint = CGPoint(x: 0, y: 0)
            switch el  {
            case .view:
                d[key] = g
            }
        }
        
        //FONTS
        for el in FontElement.allCases {
            // key is <element>"Font
            let key = el.rawValue + Category.font.rawValue
            let f = UIFont.systemFont(ofSize: 15)
            switch el  {
            case .buttonTitle:
                d[key] = f
            case .labelText:
                d[key] = UIFont.boldSystemFont(ofSize: 16)
            default: break
            }
        }
        return AXTheme.visuals(d)
    }
    
    //MARK: -  Theme for LBTA Kindle Project
    fileprivate static func buildThemeForKindle()  -> AXTheme {
        var d = [String:Any]()
        //COLORS
        for el in ColorElement.allCases {
            let key = el.rawValue + Category.color.rawValue
            switch el  {
            case .viewControllerBackground: d[key] = UIColor.axBlack001
            case .buttonBackground: d[key] = UIColor.axDarkgray
            case .buttonTitle: d[key] = UIColor.white
            case .buttonTint: d[key] = UIColor.white
            case .navigationBarTint: d[key] = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha:  1)
            case .navigationBarBarTint: d[key] = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha:  1)
            case .navigationBarLargeTitleTextAttributeForeground: d[key] = UIColor.white
            case .navigationBarTitleTextAttributeForeground: d[key] = UIColor.white
            case .navigationBarButtonItemTint: d[key] =  UIColor.white
                
            case .tableViewBackground: d[key] = UIColor(white: 1, alpha: 0.3)
            case .tableViewCellBackground: d[key] = UIColor(white: 1, alpha: 0.3)
            case .tableViewSeparator: d[key] = UIColor.axPink4
            case .tableViewHeaderBackground: d[key] = UIColor.red
            
            case .labelBackground: d[key] = UIColor.clear
                
            case .labelText: d[key] = UIColor.black
            case .tableViewFooterView: d[key] = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
            // white will force white status, anything else is default
            case .navigationBarStatus: d[key] =  UIColor.white
            case .segmentedControlTint: d[key] = UIColor.white
                
            default: break
            }
        }
        //add gradients
        for el in GradientElement.allCases {
            
            let key = el.rawValue + Category.gradient.rawValue
            let g = CAGradientLayer()
            g.colors = [UIColor.white.cgColor, UIColor.cyan.cgColor ]
            g.locations = [0.0, 1.0]
            g.startPoint = CGPoint(x: 1, y: 1.0)
            g.endPoint = CGPoint(x: 0, y: 0)
            switch el  {
            case .view:
                d[key] = g
            }
        }
        
        for el in FontElement.allCases {
            // key is <element>"Font
            let key = el.rawValue + Category.font.rawValue
            let f = UIFont.systemFont(ofSize: 15)
            switch el  {
            case .buttonTitle:
                d[key] = f
            case .labelText:
                d[key] = f
            default: break
            }
        }
        return AXTheme.visuals(d)
    }
    
    public static func build(_ style: ThemeSelector) -> AXTheme {
        
        switch style {
        case .basic:
            return buildThemeForBasic()
        case .core:
            return buildThemeForCore()
        case .kindle:
            return buildThemeForKindle()
        default:
            // everything else comes from a json file
            guard Bundle.main.path(forResource: style.fileName, ofType: "json") != nil else {
                return buildThemeForBasic() // file name not found in main bundle, return Basic theme
            }
            let defs = AxThemeVisualElementDefinitions.create(from: style.fileName)
            let dict = defs?.generateDictOfColorsFontsGradients()
            return AXTheme.visuals(dict!)
        }
    }
}



