
import UIKit

class NavigationControllerWithStatusStyleFromTheme: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return  axTheme.color(of: .navigationBarStatus) == UIColor.white ? UIStatusBarStyle.lightContent :
            UIStatusBarStyle.default
    }
}

